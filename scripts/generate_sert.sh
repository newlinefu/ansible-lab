openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout greeting.key -out greeting.crt -subj "/CN=greeting.com" \
  -addext "subjectAltName=DNS:greeting.com,DNS:www.greeting.net,IP:192.168.1.8"